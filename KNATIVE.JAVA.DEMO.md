# Vert-x

## Create a service

```bash 
export KUBECONFIG=../../config/k3s.yaml
cd services/hello-world
# create a demo namespace
#kubectl create namespace demo

export KUBE_NAMESPACE="demo"
kubectl create namespace ${KUBE_NAMESPACE} --dry-run=client -o yaml | kubectl apply -f -

```

### Login to the GitLab container registry
```bash
docker login registry.gitlab.com -u ${GITLAB_HANDLE} -p ${GITLAB_TOKEN_ADMIN}
# echo "${GITLAB_TOKEN_ADMIN}" | docker login --username k33g --password-stdin
```

### Build and push the image

```bash
# build and push the image of the application
export PATH_REGISTRY="tanooki-workshops/infra/knative-on-civo"
docker build -t registry.gitlab.com/${PATH_REGISTRY}/vert-x-rocks .
# (🖐 don't forget the `.` at the end of the command)

docker push registry.gitlab.com/${PATH_REGISTRY}/vert-x-rocks

# Test it
docker run -t -i -p 8080:8080 registry.gitlab.com/${PATH_REGISTRY}/vert-x-rocks:latest
```

### 🚀 deploy the service

```bash
kn service create java-service \
--namespace demo \
--env MESSAGE="I 💚 Knative" \
--image registry.gitlab.com/${PATH_REGISTRY}/vert-x-rocks \
--force
```

> http://java-service.demo.212.2.245.98.sslip.io

**url of the service**: http://<service_name>.<kube_namespace>.<cluster_ip>.sslip.io


Do it again with some other values

```bash
kn service create java-service \
--namespace demo \
--env MESSAGE="I 💜 Knative a lot 😍" \
--image registry.gitlab.com/${PATH_REGISTRY}/vert-x-rocks \
--force
```

```bash
kn service create java-service \
--namespace demo \
--env MESSAGE="I 💙 Knative" \
--image registry.gitlab.com/${PATH_REGISTRY}/vert-x-rocks \
--force
```

FROM gitpod/workspace-full

RUN sudo apt-get update && \
    sudo apt-get install -y sshpass && \
    brew install derailed/k9s/k9s && \
    brew install helm && \
    brew install kubernetes-cli helm && \
    brew install kompose && \
    brew install kn

USER gitpod

RUN brew install httpie




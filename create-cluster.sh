#!/bin/bash
set -o allexport; source .env; set +o allexport

echo "🤖[1/6] creating Civo Cluster: ${CLUSTER_NAME}" 

civo apikey save civo-key ${CIVO_API_KEY}

#civo apikey add civo-key ${CIVO_API_KEY}
civo apikey current civo-key
civo kubernetes create ${CLUSTER_NAME} --create-firewall --size=${CLUSTER_SIZE} --nodes=${CLUSTER_NODES} --region=${CLUSTER_REGION} --remove-applications Traefik --wait

# Cluster size 
# g4s.kube.xsmall g4s.kube.small g4s.kube.medium g4s.kube.large
# g4p.kube.small g4p.kube.medium g4p.kube.large g4p.kube.xlarge
# g4c.kube.small g4c.kube.medium g4c.kube.large g4c.kube.xlarge
# g4m.kube.small g4m.kube.medium g4m.kube.large g4m.kube.xlarge

echo "📝[2/6]  save the KUBECONFIG file of the ${CLUSTER_NAME} cluster to ./config/k3s.yaml "

civo apikey add civo-key "${CIVO_API_KEY}"
civo apikey current civo-key
civo --region=${CLUSTER_REGION} kubernetes config ${CLUSTER_NAME} > ./config/k3s.yaml

echo "🌍[3/6]  get the cluster url (./config/url.txt)"

URL=$(kubectl cluster-info | grep -E 'Kubernetes master|Kubernetes control plane' | awk '/http/ {print $NF}') 
# Removing ANSI color codes from text stream
NEW_URL=$(echo $URL | sed 's/\x1b\[[0-9;]*m//g')
echo "$NEW_URL"
echo "$NEW_URL" > ./config/url.txt

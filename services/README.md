

## Create Vert-x application

- See https://gitlab.com/k33g_org/k33g_org.gitlab.io/-/issues/13

```bash
cd services

curl -G https://start.vertx.io/starter.zip \
-d "groupId=garden.bots" \
-d "artifactId=hello-world" \
-d "vertxVersion=4.2.4" \
-d "vertxDependencies=vertx-web" \
-d "language=java" \
-d "jdkVersion=11" \
-d "buildTool=maven" \
--output hello-world.zip

unzip hello-world.zip -d ./hello-world

#rm hello-world.zip
cd hello-world
./mvnw clean compile exec:java
```

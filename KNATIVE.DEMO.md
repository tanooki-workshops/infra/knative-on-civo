## Create a service

```bash 
export KUBECONFIG=../../config/k3s.yaml
cd services/hello
# create a demo namespace
#kubectl create namespace demo

export KUBE_NAMESPACE="demo"
kubectl create namespace ${KUBE_NAMESPACE} --dry-run=client -o yaml | kubectl apply -f -

```

### Login to the GitLab container registry
```bash
docker login registry.gitlab.com -u ${GITLAB_HANDLE} -p ${GITLAB_TOKEN_ADMIN}
# echo "${GITLAB_TOKEN_ADMIN}" | docker login --username k33g --password-stdin
```

### Build and push the image

```bash
# build and push the image of the application
export PATH_REGISTRY="tanooki-workshops/infra/knative-on-civo"
docker build -t registry.gitlab.com/${PATH_REGISTRY}/awesome-web-app .
# (🖐 don't forget the `.` at the end of the command)

docker push registry.gitlab.com/${PATH_REGISTRY}/awesome-web-app
```

### 🚀 deploy the service

```bash
kn service create my-service \
--namespace demo \
--env MESSAGE="I 💚 Knative" \
--env BACKGROUND_COLOR="mediumslateblue" \
--image registry.gitlab.com/${PATH_REGISTRY}/awesome-web-app \
--force
```

> http://my-service.demo.212.2.247.39.sslip.io

**url of the service**: http://<service_name>.<kube_namespace>.<cluster_ip>.sslip.io


Do it again with some other values

```bash
kn service create my-service \
--namespace demo \
--env MESSAGE="I 💜 Knative a lot 😍" \
--env BACKGROUND_COLOR="yellow" \
--image registry.gitlab.com/${PATH_REGISTRY}/awesome-web-app \
--force
```

```bash
kn service create my-service \
--namespace demo \
--env MESSAGE="I 💙 Knative" \
--env BACKGROUND_COLOR="orange" \
--image registry.gitlab.com/${PATH_REGISTRY}/awesome-web-app \
--force
```
